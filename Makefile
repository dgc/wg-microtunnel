all: ami

microtunnel.json: microtunnel.yml
	./jx -pj microtunnel.yml >microtunnel.json

validate: microtunnel.json
	packer validate microtunnel.json

files.tar: files
	(cd files; tar cf ../files.tar .)

ami: microtunnel.json files.tar
	packer build microtunnel.json

test:
	aws ec2 run-instances \
		--image-id $(AWS_AMI) \
		--key-name $(AWS_KEY) \
		--security-group-ids $(AWS_SG) \
		--user-data file://example-userdata.yml \
		--instance-type t3.nano \
		--subnet-id $(AWS_SUBNET) \
		--associate-public-ip-address \
		--generate-cli-skeleton

