#!/bin/sh

[ $(id -u) = 0 ] || exec sudo "$0" "$@"

(
	mkdir /µ
	cd /µ
	tar xf /tmp/files.tar
	chmod 755 wireguard-init
	chmod 755 wg-pubkey
	chmod 755 wg-userdata
)

echo http://dl-cdn.alpinelinux.org/alpine/edge/testing >>/etc/apk/repositories
apk update
apk add curl
apk add py3-yaml
apk add py3-requests
apk add socat
apk add wireguard-virt
apk add wireguard-tools

touch /etc/iptables/rules-save

ln -s /µ/wireguard-init /etc/init.d
rc-update add iptables
rc-update add wireguard-init
